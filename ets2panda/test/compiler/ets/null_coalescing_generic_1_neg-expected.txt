{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "fos",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 10
                },
                "end": {
                  "line": 16,
                  "column": 13
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "fos",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 10
                    },
                    "end": {
                      "line": 16,
                      "column": 13
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a0",
                      "typeAnnotation": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "T",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 16,
                                "column": 38
                              },
                              "end": {
                                "line": 16,
                                "column": 39
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 16,
                              "column": 38
                            },
                            "end": {
                              "line": 16,
                              "column": 40
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 16,
                            "column": 38
                          },
                          "end": {
                            "line": 16,
                            "column": 40
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 16,
                          "column": 34
                        },
                        "end": {
                          "line": 16,
                          "column": 40
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 16,
                        "column": 34
                      },
                      "end": {
                        "line": 16,
                        "column": 40
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSTypeReference",
                  "part": {
                    "type": "ETSTypeReferencePart",
                    "name": {
                      "type": "Identifier",
                      "name": "T",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 16,
                          "column": 42
                        },
                        "end": {
                          "line": 16,
                          "column": 43
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 16,
                        "column": 42
                      },
                      "end": {
                        "line": 16,
                        "column": 45
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 42
                    },
                    "end": {
                      "line": 16,
                      "column": 45
                    }
                  }
                },
                "typeParameters": {
                  "type": "TSTypeParameterDeclaration",
                  "params": [
                    {
                      "type": "TSTypeParameter",
                      "name": {
                        "type": "Identifier",
                        "name": "T",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 16,
                            "column": 14
                          },
                          "end": {
                            "line": 16,
                            "column": 15
                          }
                        }
                      },
                      "constraint": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "Integral",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 16,
                                "column": 24
                              },
                              "end": {
                                "line": 16,
                                "column": 32
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 16,
                              "column": 24
                            },
                            "end": {
                              "line": 16,
                              "column": 33
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 16,
                            "column": 24
                          },
                          "end": {
                            "line": 16,
                            "column": 33
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 16,
                          "column": 14
                        },
                        "end": {
                          "line": 16,
                          "column": 33
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 13
                    },
                    "end": {
                      "line": 16,
                      "column": 33
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "myval",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "Short",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 17,
                                      "column": 16
                                    },
                                    "end": {
                                      "line": 17,
                                      "column": 21
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 17,
                                    "column": 16
                                  },
                                  "end": {
                                    "line": 17,
                                    "column": 23
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 17,
                                  "column": 16
                                },
                                "end": {
                                  "line": 17,
                                  "column": 23
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 17,
                                "column": 9
                              },
                              "end": {
                                "line": 17,
                                "column": 14
                              }
                            }
                          },
                          "init": {
                            "type": "NumberLiteral",
                            "value": 2,
                            "loc": {
                              "start": {
                                "line": 17,
                                "column": 24
                              },
                              "end": {
                                "line": 17,
                                "column": 25
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 17,
                              "column": 9
                            },
                            "end": {
                              "line": 17,
                              "column": 25
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 17,
                          "column": 5
                        },
                        "end": {
                          "line": 17,
                          "column": 26
                        }
                      }
                    },
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "LogicalExpression",
                        "operator": "??",
                        "left": {
                          "type": "Identifier",
                          "name": "a0",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 12
                            },
                            "end": {
                              "line": 18,
                              "column": 14
                            }
                          }
                        },
                        "right": {
                          "type": "Identifier",
                          "name": "myval",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 18
                            },
                            "end": {
                              "line": 18,
                              "column": 23
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 12
                          },
                          "end": {
                            "line": 18,
                            "column": 23
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 5
                        },
                        "end": {
                          "line": 18,
                          "column": 24
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 44
                    },
                    "end": {
                      "line": 19,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 13
                  },
                  "end": {
                    "line": 19,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 16,
                  "column": 13
                },
                "end": {
                  "line": 19,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 1
              },
              "end": {
                "line": 19,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 20,
      "column": 1
    }
  }
}
TypeError: Type 'T|Short' is not compatible with the enclosing method's return type 'T' [null_coalescing_generic_1_neg.ets:18:12]
